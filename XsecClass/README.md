how to run:

make clean 

make 

#inclusive 

./WorkSpaceMaker  --Variable pt  --do2D --doFit

#inclusive splitting 2l2l and 2l2l'

./WorkSpaceMaker  --Variable pt  --do2D --doFit --chSplit 1

#inclusive splitting in all final states

./WorkSpaceMaker  --Variable pt  --do2D --doFit --chSplit 2

#single channel

./WorkSpaceMaker  --Variable pt  --do2D --doFit --Channel 4mu

#fiducial 

./WorkSpaceMaker  --FidTotal --doFit

./WorkSpaceMaker  --FidSum --doFit

./WorkSpaceMaker  --Fid4l --doFit

./WorkSpaceMaker  --Fid2l2l --doFit

./WorkSpaceMaker  --FidComb --doFit

./WorkSpaceMaker  --FidChan --doFit

./WorkSpaceMaker  --BRDiff --doFit

P.S. for m12, m34, m12m34 the --do2D option is NOT available

