#include "InputMaker.h"

void InputMaker :: LoadAllFactors() {
	
	LoadAFactors();
	LoadCFactors();
	LoadRFactors();
	LoadMinitreeN();
	LoadNbinbybin();
}

void InputMaker :: LoadCFactors(){

  if( m_debug )
    cout << "<LoadCFactors()>:: loading correction factor values" << endl;

  m_cFactors.clear();
  m_InStream.open( (m_cfFile).c_str() );
  if (m_debug) cout << "opening file: " << (m_cfFile).c_str() << endl; 
 
  LoadValues(m_cFactors);
  m_InStream.close();

  if( m_verbose ){
    cout << "<LoadCFactors()>:: Printing loaded values " << endl;
    PrintValues(m_cFactors, false);
  }
  
}

void InputMaker :: LoadAFactors(){

  if( m_debug )
    cout << "<LoadAFactors()>:: loading acceptance values "  << endl;

  m_aFactors.clear();
  m_InStream.open( (m_afFile).c_str() );
  if (m_debug) cout << "opening file: " << (m_afFile).c_str() << endl; 
  
  LoadValues(m_aFactors);
  m_InStream.close();

  if( m_verbose ){
    cout << "<LoadAFactors()>:: Printing loaded values " << endl;
    PrintValues(m_aFactors, false);
  }
      
}

void InputMaker :: LoadRFactors(){

  if( m_debug )
    cout << "<LoadRFactors()>:: loading prodution mode ratio values "  << endl;

  m_rFactors.clear();
  m_InStream.open( (m_rfFile).c_str() );
  if (m_debug) cout << "opening file: " << (m_rfFile).c_str() << endl; 

  LoadValues(m_rFactors);
  
  m_InStream.close();

  if( m_verbose ){
    cout << "<LoadRFactors()>:: Printing loaded values " << endl;
    PrintValues(m_rFactors, false);
  }
}

void InputMaker :: LoadMinitreeN(){
  
  if( m_debug )
    cout << "<LoadMinitreeN()>:: loading N exp from minitree "  << endl;
  
  m_Nminitree.clear();
  m_InStream.open( (m_minitreeNFile).c_str() );
  if (m_debug) cout << "opening file: " << (m_minitreeNFile).c_str() << endl; 
  
  LoadValues(m_Nminitree);
  
  m_InStream.close();
  
  if( m_verbose ){
    cout << "<LoadMinitreeN>:: Printing loaded values " << endl;
    PrintValues(m_Nminitree, false);
  }
}

void InputMaker :: LoadNbinbybin(){
  
  if( m_debug )
    cout << "<LoadNbinbybin()>:: loading N exp from binbybin method "  << endl;
  
  m_Nbinbybin.clear();
  m_InStream.open( (m_binbybinNFile).c_str() );
  if (m_debug) cout << "opening file: " << (m_binbybinNFile).c_str() << endl; 
  
  LoadValues(m_Nbinbybin);
  
  m_InStream.close();
  
  if( m_verbose ){
    cout << "<LoadNbinbybin>:: Printing loaded values " << endl;
    PrintValues(m_Nbinbybin, false);
  }
}

void InputMaker :: LoadfNonfid(){
  
  if( m_debug )
    cout << "<LoadfNonfid()>:: loading fNonfid "  << endl;
  
  m_fNonfid.clear();
  m_InStream.open( (m_fNfFile).c_str() );
  if (m_debug) cout << "opening file: " << (m_fNfFile).c_str() << endl; 
  
  LoadValues(m_fNonfid);
  
  m_InStream.close();
  
  if( m_verbose ){
    cout << "<LoadfNonfid>:: Printing loaded values " << endl;
    PrintValues(m_fNonfid, false);
  }
}

void InputMaker :: LoadRspMtx(){
  
  if( m_debug )
    cout << "<LoadRspMtx()>:: loading response matrix "  << endl;
  
//  m_rspMtx.clear();
  m_InStream.open( (m_rspMtxFile).c_str() );
  if (m_debug) cout << "opening file: " << (m_rspMtxFile).c_str() << endl; 
  
  LoadValues(m_rspMtx);
  
  m_InStream.close();
  
  if( m_verbose ){
    cout << "<LoadfNonfid>:: Printing loaded values " << endl;
    PrintValues(m_rspMtx, false);
  }
}

void InputMaker :: LoadValues(vector<vector<double>> &vec) {
  
	if (m_debug) cout << "<LoadValues()>:: loading values: " << m_Variable << " var " << endl;

	double val = 0, err = 0;
	std::string tmp = "";
	int line = 0;
	int numItems = 0;
	
	while (!m_InStream.eof()) {
		getline(m_InStream,tmp);
		++numItems;
	}

	if (m_debug) cout << "numItems: " << numItems << endl;	
	int cols = numItems-1;// # of columns found
	//bool isNotMatrix= false;
	line = 0;
	
	m_InStream.seekg(m_InStream.beg); // reset ifstream
	while (m_InStream.good()) 
	{
		val = 0, err = 0;
		if (line == 0) getline(m_InStream,tmp);
		if (m_debug) cout << "line: " << line << endl;	
	  
		m_InStream >> tmp;
	  
		for (int ii = 0; ii < cols; ++ii) 
		{
			m_InStream >> val; 
			m_InStream >> err;
	    		if (m_debug) cout << " value " << val << "   " << line << "   "<< ii << endl;
	      		//data[m_ChNum].push_back( make_pair(val,err) );
	      		vec[line].push_back(val);
	  	}
	
		line++;
	}

}

void InputMaker :: LoadValues(double**& mtx) {
  
	if (m_debug) cout << "<LoadValues()>:: loading matrix values: " << m_Variable << " var " << endl;
	double val = 0, err = 0;
	std::string tmp = "";
	int line = 0;
	int numItems = 0;
	
	//while(!m_InStream.eof()) {
	//	getline(m_InStream, tmp);
	//	++numItems;
	// }
	//int cols = numItems-1;// # of columns found
	int cols = m_binnum;
	cout << "numItems: " << numItems << endl;
	cout << "cols: " << cols << endl;
	line = 0;
	
	m_InStream.seekg( m_InStream.beg ); // reset ifstream
	while(m_InStream.good()){
		if (m_debug) cout << "instream good" << endl;
		val = 0, err = 0;
		if (m_debug) cout << "val = " << val << ", err = " << err << endl;
		if( line==0 ) getline(m_InStream,tmp);
	  
		m_InStream >> tmp;
		if (m_debug) cout << "tmp: " << tmp << endl;
	  
		for (int ii = 0; ii < cols; ++ii) {
			if (m_debug) cout << "ii: "<< ii << endl;
			if (m_debug) cout << "val = " << val << ", err = " << err << endl;
	  		m_InStream >> val; m_InStream >> err;
	  		mtx[line][ii] = val;
			if (m_debug) cout << "line: " << line << ", mtx[line][ii] = " << val << endl;
		}	
		line++;
		if (line == m_binnum) break;
	}
	
}

void InputMaker :: LoadValues(vector<double> &vec) {
  
	if (m_debug) cout << "<LoadValues()>:: loading values: " << m_Variable << " var " << endl;
	
	double val = 0, err = 0;
	std::string tmp = "";
	int line = 0;
	int numItems = 0;
	
	int cols = numItems-1;// # of columns found
	cols = 1;
	line = 0;
	
	m_InStream.seekg(m_InStream.beg); // reset ifstream
	while (m_InStream.good())
	{
		val = 0, err = 0;
		if (line == 0) getline(m_InStream,tmp);
		
		m_InStream >> tmp;
		
		for (int ii = 0; ii < cols; ++ii)
		{
			m_InStream >> val; 
			m_InStream >> err;
			if (m_debug) cout << " value " << val << "   " << line << "   "<< ii << endl;
			//data[m_ChNum].push_back( make_pair(val,err) );
			vec.push_back(val);
		}
	
		line++;
	}

}

void InputMaker :: PrintValues(vector<double>& vals, bool save) {

	int lim = 0;
	if (m_Variable == "pt") lim = m_binnum - 1;
	else lim = m_binnum;
	if (m_doFiducial) lim = m_binnum;

	if (save) m_OutStream << "bin             value" << std::endl;

	for (int i = 0; i < lim; i++) {
		if (m_doFiducial) {
			std::cout << m_BinLabels[i] << "         " << vals[i] << "  0.000000  " << std::endl;
			if (save) m_OutStream << m_BinLabels[i] << "         " << vals[i] << "  0.000000  ";
			if (i < m_binnum -1) m_OutStream << "\n";
		}
		else {
			std::cout << i << "         " << vals[i] << "  0.000000  " << std::endl;
			if (save) m_OutStream << i << "         " << vals[i] << "  0.000000  ";
			if (i < m_binnum -1) m_OutStream << "\n";
		}
	}	
}

void InputMaker :: PrintValues(double**& mtx, bool save) {

	int lim = 0;
	if (m_Variable == "pt") lim = m_binnum - 1;
	else lim = m_binnum;
	if (m_doFiducial) lim = m_binnum;

	if (save) m_OutStream << "bin             value" << std::endl;
	for (int i = 0; i < lim; i++) {
		std::cout << i << "   ";
		if (save) m_OutStream << i << "   ";
		for (int j = 0; j < lim; j++) {
			std::cout << "  " << mtx[i][j] << "  0.000000";
			if (save) m_OutStream << "  " << mtx[i][j] << "  0.000000";
		}
		if (i < m_binnum -1) m_OutStream << "\n";
		cout <<endl;
	}		
}

