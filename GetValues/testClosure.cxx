#include <fstream>
#include <iostream>
#include <fstream>
#include <vector>
#include "TString.h"
#include <string>
#include <stdlib.h>
#include "TTree.h"
#include "TFile.h"
#include "Riostream.h"
#include "InputMaker.h"

void InputMaker :: printClosure()
{
	std::cout << setprecision(6) << std::endl;
	std::cout << "N: " << std::endl;
	std::cout << "bin            RspMatrix             BinbyBin             error [%]" << std::endl;
	for (int i = 0; i < m_binnum; i++) {
		std::cout << i << "         " << m_Nrspmtx[i] << "  0.000000  " << m_Nbinbybin[i] << "  0.000000  " << 100*abs(m_Nrspmtx[i]-m_Nbinbybin[i])/m_Nrspmtx[i] << "  0.000000" << std::endl;
	}	
	
	std::cout << setprecision(6) << std::endl;
	std::cout << "N: " << std::endl;
	std::cout << "bin            RspMatrix             Nminitree             error [%]" << std::endl;
	for (int i = 0; i < m_binnum; i++) {
		std::cout << i << "         " << m_Nrspmtx[i] << "  0.000000  " << m_Nminitree[i] << "  0.000000  " << 100*abs(m_Nrspmtx[i]-m_Nminitree[i])/m_Nminitree[i] << "  0.000000" << std::endl;
	}	
	
	std::cout << setprecision(6) << std::endl;
	std::cout << "N: " << std::endl;
	std::cout << "bin            BinbyBin             Nminitree             error [%]" << std::endl;
	for (int i = 0; i < m_binnum; i++) {
		std::cout << i << "         " << m_Nbinbybin[i] << "  0.000000  " << m_Nminitree[i] << "  0.000000  " << 100*abs(m_Nbinbybin[i]-m_Nminitree[i])/m_Nminitree[i] << "  0.000000" << std::endl;
	}	

	if (m_doFiducial) m_OutStream.open("Fid/"+m_ProdMode+"pred.dat");
	else m_OutStream.open("predictions/"+m_Variable+m_ProdMode+"pred.dat");
	std::cout << std::endl;
	std::cout << "Prediction/bin width" << std::endl;
	m_OutStream << "val val" << endl;
	double scale = 1.0;
	if (m_ProdMode == "VBF") scale = 80.0;
	if (m_ProdMode == "WH" || m_ProdMode == "ZH") scale = 1000.0;
	if (m_ProdMode == "MG5") scale = 48600.0*0.000124/4.1138;
	for (unsigned int i = 0; i < m_binnum; i++) {
		double binWidth = m_bins[i+1] - m_bins[i];
		std::cout << "bin width: " << binWidth << std::endl;
		std::cout << i << "         " << scale*m_aFactors[i]*m_XS*m_BR*m_rFactors[i]/binWidth << "  0.000000  " << std::endl;
		if (m_debug) std::cout << i << "         " << scale << "*" << m_aFactors[i] << "*" << m_XS << "*" << m_BR << "*" << m_rFactors[i] << "/" << binWidth << "  0.000000  " << std::endl;
		m_OutStream << scale*m_aFactors[i]*m_XS*m_BR*m_rFactors[i]/binWidth << "  0.000000  ";
		if (i < m_binnum -1) m_OutStream << "\n";
	}	
	m_OutStream.close();

	if (m_doPlots) {
//		makePlot(rspMtx, prodType);
	}
}


void InputMaker :: fillNrspmtx() 
{
	for (int i = 0; i < m_binnum; i++)
	{
		double Nexp = 0.0;
		for (int j = 0; j < m_binnum; j++)
		{
			double rm = m_rspMtx[i][j];
			if (m_debug) {
				std::cout << "i,j: " << i << "," << j << ". rspmtx[i,j]: " << rm << endl;
				std::cout << "fnonfid[i] = " << m_fNonfid[i] << endl;
				std::cout << "aFactors[j] = " << m_aFactors[j] << endl;
				std::cout << "rFactors[i] = " << m_rFactors[j] << endl;
				std::cout << "Nexp: " << Nexp << endl;
			}
			Nexp = Nexp + rm*(1+m_fNonfid[i])*m_XS*m_BR*m_aFactors[j]*m_rFactors[j];
			//Nexp = Nexp + rm*(1)*XS*BR*LU*aFs[j]*rFs[j];
			//Nexp = Nexp + rm*(fN[i])*XS*BR*LU*aFs[j]*rFs[j];
		}

		if (m_debug || m_verbose) std::cout << "Nrsmptx: " << Nexp << std::endl;
		if (m_debug || m_verbose) std::cout << std::endl;
		m_Nrspmtx[i] = Nexp;	
	}		
}


void InputMaker :: compareCFmtx()
{
	cout << endl;
	cout << "bin	RspMtx col. sum		CF	    ratio        error[%]" << endl;
	for (int i = 0; i < m_binnum; i++) 
	{
		double sum = 0.0;
		for (int j = 0; j < m_binnum; j++) { sum += m_rspMtx[i][j]; }
		cout << i << "	   " << sum << "           " << m_cFactors[i] << "     " << sum/m_cFactors[i] << "       " <<  100*(1-sum/m_cFactors[i]) << endl;
	}
}


/*
void InputMaker :: makePlot()
{
        gStyle->SetPaintTextFormat("2.1f %%");
        gStyle->SetOptStat(0);

       	
	TString yAxisLabel = "truth p_{T}^{4l} [GeV]";
	TString xAxisLabel = "reco p_{T}^{4l} [GeV]";
	int kNumHists = 1;
		
	TH2F* tmpHist = new TH2F("", "", m_binnum, 0, m_binnum, m_binnum, 0, m_binnum);
	double val =0;
	for (int i = 0; i < m_binnum; ++i) {
	
		tmpHist->GetXaxis()->SetBinLabel(i+1, pTbinList[i]);
		tmpHist->GetYaxis()->SetBinLabel(i+1, pTbinList[i]);
		
		float norm = 0;
		for (int j = 0; j < m_binnum; j++) { 
			tmpHist->SetBinContent(j+1, i+1, mtx[i][j]); 
			norm += mtx[i][j]*(m_bins[j+1]-m_bins[j]); }
		
		for (int j = 0; j < m_binnum; j++){
		        val = (100.*mtx[i][j]*(m_bins[j+1]-m_bins[j])/norm < 0.1) ? 0 : 100.*mtx[i][j]*(m_bins[j+1]-m_bins[j])/norm; //do not plot if the migration is < 0.1
                  	tmpHist->SetBinContent(j+1, i+1, val); }
	}
	
	
	std::cout << "Reco Bin	 Truth Bin   Response" << std::endl;	
	for (int i = 0; i < m_binnum; i++) {
		for (int j = 0; j < m_binnum; j++) {
			std::cout << i << "	" << j << "	" << tmpHist->GetBinContent(i, j) << std::endl;
		}
	}
	
	TCanvas *canvas = new TCanvas("c1","c1",0,0,600,600);
	canvas->SetRightMargin(0.2);
	canvas->cd();
	
	tmpHist->GetXaxis()->SetTitle(xAxisLabel);
	tmpHist->GetYaxis()->SetTitle(yAxisLabel);
	tmpHist->GetXaxis()->SetLabelSize(0.2);
	tmpHist->GetYaxis()->SetLabelSize(0.2);
	tmpHist->SetMaximum(100.0);
	tmpHist->SetMinimum(0.0);
	tmpHist->SetMarkerSize(1.);
	
	canvas->SetGridx(1);
	canvas->SetGridy(1);
	gStyle->SetPalette(1);
	tmpHist->Draw("COLZTEXT");
	//ATLASLabel(0.20, 0.875, "Internal", 1);
	tmpHist->GetXaxis()->SetLabelSize(0.03);
	tmpHist->GetYaxis()->SetLabelSize(0.03);
	canvas->SaveAs("plots/"+prodType+"_response.pdf");
}
*/


