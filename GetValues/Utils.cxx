#include "InputMaker.h"
#include "InputValues.cxx"

using namespace std;

void InputMaker :: InitializeIO(){

	if( m_debug ) cout << "<InitializeIO()>:: Initializing I/O objects/values " << endl;

	DefineInputData();
	if (m_doComparison && !m_doFactors) {  
		LoadCFactors();
		LoadAFactors();
		LoadRFactors();
		LoadNbinbybin();
		LoadMinitreeN();
	}
	if (m_doComparison && !m_doMatrix) {	
		LoadfNonfid();
		LoadRspMtx();
	}
}

void InputMaker :: initializeFactorVectors()
{
	for (int i = 0; i < m_binnum; i++)
	{
		m_aFactors.push_back(0.0);
		m_cFactors.push_back(0.0);
		m_rFactors.push_back(0.0);
		m_Nminitree.push_back(0.0);
		m_Nbinbybin.push_back(0.0);
	}
	
	if (m_debug) cout << "Initialized vectors for calcFactors" << endl; 
}

void InputMaker :: initializeMatrixVectors()
{
	//std::vector<double>* tmp;
	//m_rspMtx = new double[11][11];
	m_rspMtx = new double*[m_binnum];
	m_cFmtx = new double*[m_binnum];
	for (int i = 0; i < m_binnum; i++)
	{
		//std::vector<double> *tmp = new vector<double>;
		m_rspMtx[i] = new double[m_binnum];
		m_cFmtx[i] = new double[m_binnum];
		for (int j = 0; j < m_binnum; j++) {
			m_rspMtx[i][j] = 0.0;
			m_cFmtx[i][j] = 0.0;
			if (m_debug) cout << "initialize mtx bin "<<i<<", "<<j<<" to " << m_rspMtx[i][j] << endl;
		}
		m_fNonfid.push_back(0.0);
		m_Nrspmtx.push_back(0.0);
		//m_rspMtx[i] = new vector<double>(m_binnum, 0.0);
	}

	//for (unsigned int i = 0; i < m_binnum; i++) m_rspMtx.push_back(tmp);
	//m_rspMtx(m_binnum, std::vector<double>(m_binnum, 0.0));
	if (m_debug) cout << "Initialized vectors for responseMatrix" << endl; 
}


int InputMaker :: getBin(float val)
{
	//if (val == -nan) return -1;

	if (m_doFiducial) { if (val > 3) return -1; }
	if (m_Variable == "pt") { if (val > 1000.0) return -1; }
	if (m_Variable == "y") { if (val > 2.5) return -1; }
	if (m_Variable == "m12") { if (val > 106) return -1; }
	if (m_Variable == "m34") { if (val > 65) return -1; }
	if (m_Variable == "mjj") { if (val > 3000) return -1; }
	if (m_Variable == "ljpt"){ if (val > 350) return -1; }
	
	for (int i = m_binnum - 1; i >= 0; i--)
	{
		if (val >= m_bins[i]) return i;
	}

	return -1;
}


TString InputMaker :: GetFidMinitreeVar() {

	if (m_Variable == "pt") return "higgs_pt";
	if (m_Variable == "njets") return "n_jets";
	if (m_Variable == "y") return "higgs_y";
	if (m_Variable == "m12" ) return "Z1_m";
	if (m_Variable == "m34" ) return "Z2_m";
	if (m_Variable == "mjj" ) return "dijet_m";
	if (m_Variable == "ljpt") return "leading_jet_pt";

	return "";
}

TString InputMaker :: GetRecoMinitreeVar() {

	if (m_Variable == "pt") return "pt4l_fsr";
	if (m_Variable == "njets") return "n_jets";
	if (m_Variable == "y") return "y4l_fsr";
	if (m_Variable == "m12" ) return "mZ1_fsr";
	if (m_Variable == "m34" ) return "mZ2_fsr";
	if (m_Variable == "mjj" ) return "dijet_invmass";
	if (m_Variable == "ljpt") return "leading_jet_pt";

	return "";
}

void InputMaker :: DefineInputData(){

	if (m_debug) cout << "<DefineInputData>:: Defining input cF, aF, rF, mtx data files " << endl;
        if (m_doFiducial) m_InputDir = m_InputDir + "Fid/";

	m_cfFile = m_InputDir+m_Variable+"/cF_"+m_ProdMode+".dat";
	m_afFile = m_InputDir+m_Variable+"/aF_"+m_ProdMode+".dat";
	m_rfFile = m_InputDir+m_Variable+"/rF_"+m_ProdMode+".dat";
	m_minitreeNFile = m_InputDir+m_Variable+"/Nminitree_"+m_ProdMode+".dat";
	m_binbybinNFile = m_InputDir+m_Variable+"/Nbinbybin_"+m_ProdMode+".dat";
	m_rspmtxNFile = m_InputDir+m_Variable+"/Nrspmtx_"+m_ProdMode+".dat";
	m_fNfFile = m_InputDir+m_Variable+"/fNonfid_"+m_ProdMode+".dat";
	m_rspMtxFile = m_InputDir+m_Variable+"/rspmtx_"+m_ProdMode+".dat";
}

string InputMaker :: GetCurrentWorkingDir() {
  
  char buff[FILENAME_MAX];
  //GetCurrentDir( buff, FILENAME_MAX );
  std::string current_working_dir(buff);
  
  return current_working_dir;
  
}

void InputMaker :: CreateOutputDir(){

  struct stat st;
  if (m_doFiducial) m_OutputDir = m_OutputDir + "Fid/";
  else m_OutputDir = "./"+m_OutputDir+m_Variable+"/";
  if(stat(m_OutputDir.c_str(),&st) == 0){
    cout << "\033[1;39m <CreateOutputDir()>:: \033[0m"
	 << "\033[1;32m Out Directory " << m_OutputDir << " already present!\033[0m" << endl;
  }
  else{
    cout << "\033[1;39m <CreateOutputDir()>:: \033[0m"
	 << "\033[1;34m Creating Directory " << m_OutputDir << " ... \033[0m" << endl;
    int outD = system( ("mkdir "+m_OutputDir).c_str() );
    if(outD!=0)
      cout << "\033[1;39m <CreateOutputDir()>:: \033[0m"
	   << "\033[1;31m Directory " << m_OutputDir << " could not be created!\033[0m" << endl;
  }
  
}


inline const char* InputMaker :: GetChannel(int n){

  switch(n){
  case Channel::AllChannels: return "incl";
  case Channel::FourMu     : return "4mu";
  case Channel::FourEl     : return "4e";
  case Channel::TwoMuTwoEl : return "2mu2e";
  case Channel::TwoElTwoMu : return "2e2mu";
  default                  : return "";
  }
  
}

inline const char* InputMaker :: GetProcess(int p){

  switch(p){
  case Process::Signal: return "AllSig";
  case Process::ggH   : return "ggH";
  case Process::VBF   : return "VBF";
  case Process::WH    : return "WH";
  case Process::ZH    : return "ZH";
  case Process::ttH   : return "ttH";
  case Process::bbH   : return "bbH";
  case Process::ggZZ  : return "ggZZ";
  case Process::qqZZ  : return "qqZZ";
  case Process::redBkg: return "redBkg";
  default             : return "";
  }
  
}

string InputMaker :: GetVariable(int v){

  switch(v){
  case Variable::pt          : return "pt";
  case Variable::pt_0jet     : return "pt0j";
  case Variable::pt_1jet     : return "pt1j";
  case Variable::pt_2jet     : return "pt2j";
  case Variable::m12         : return "m12";
  case Variable::m34         : return "m34";
  case Variable::m12m34      : return "m12m34";
  case Variable::y           : return "y";
  case Variable::njet        : return "njet";
  case Variable::njetv2      : return "njetv2";
  case Variable::njetv3      : return "njetv3";
  case Variable::nbjet       : return "nbjet";
  case Variable::costhetastar: return "cts";
  case Variable::LeadJetPt   : return "ljpt";
  case Variable::mjj         : return "mjj";
  case Variable::deltaetajj  : return "etajj";
  case Variable::deltaphijj  : return "phijj";
  default                    : return "";
  }
  
}

int InputMaker :: GetChannelId(string ch){

  if( m_debug )
    cout << "<GetChannelId>:: retrieving channel Id" << endl;

  int m_Id=-1;
  if(      ch=="incl"  ) m_Id = Channel::AllChannels;
  else if( ch=="4mu"   ) m_Id = Channel::FourMu     ;
  else if( ch=="4e"    ) m_Id = Channel::FourEl     ;
  else if( ch=="2mu2e" ) m_Id = Channel::TwoMuTwoEl ;
  else if( ch=="2e2mu" ) m_Id = Channel::TwoElTwoMu ;

  return m_Id;
  
}


string InputMaker :: ConvertToStringWithPrecision(double value, int n){
  
  ostringstream out;
  out << setprecision(n) << value;
  return out.str();
  
}

void InputMaker :: drawlatex(TLatex *t){

  t->SetTextColor(kRed+2);
  t->SetTextFont(43);
  t->SetTextSize(22);
  t->Draw();

}

void InputMaker :: drawATLASInternal(){

  TLatex AT;
  AT.SetNDC();
  AT.SetTextFont(72);
  AT.SetTextColor(kBlack);
  AT.DrawLatex(0.25,0.89,"ATLAS");

  TLatex Lab;
  Lab.SetNDC();
  Lab.SetTextFont(42);
  Lab.SetTextColor(kBlack);
  Lab.DrawLatex(0.40,0.89, "Internal");
    
}

void InputMaker :: drawEnergyLumi(){

  TLatex lumi;
  lumi.SetNDC();
  lumi.SetTextFont(42);
  lumi.SetTextSize(0.033);
  lumi.SetTextColor(kBlack);
  lumi.DrawLatex(0.25,0.77,("#sqrt{s} = 13 TeV, "+ConvertToStringWithPrecision(m_lumi, 3)+" fb^{-1}").c_str());
  
}

void InputMaker :: drawChannel(){

  TLatex Chan;
  Chan.SetNDC();
  Chan.SetTextFont(42);
  Chan.SetTextSize(0.033);
  Chan.SetTextColor(kBlack);
  Chan.DrawLatex( 0.25,0.83,"H #rightarrow ZZ* #rightarrow 4l" );

}

void InputMaker :: DrawSigmaLines (TF1 *sigma){
  
  sigma->SetLineStyle(2);
  sigma->SetLineWidth(1);
  sigma->SetLineColor(kRed);
  sigma->Draw("same");
  
}

void InputMaker :: CreateProcList(){

//  m_process["Signal"] = 0;
//  m_process["ggH"]    = 1;
//  m_process["VBF"]    = 2;
//  m_process["WH"]     = 3;
//  m_process["ZH"]     = 4;
//  m_process["ttH"]    = 5;
//  m_process["bbH"]    = 6;
//  m_process["ggZZ"]   = 7;
//  m_process["qqZZ"]   = 8;
//  m_process["redBkg"]  = 9;

  
}

//void InputMaker :: CleanUp(){
//
//  cout << "\033[1;32m <CleanUp()>:: cleaning memory ... \033[0m" << endl;
//  
//  m_aFactors.clear();
//  m_cFactors.clear();
//  m_rFactors.clear();
//  m_Nminitree.clear();
//  m_Nbinbybin.clear();
//  m_Nrspmtx.clear();
//  m_rspMtx.clear();
//}

void InputMaker :: SetCanvasSize(int *cw, int *ch){
  
  cout << "\033[1;32m <SetCanvasSize()>\033[0m" << endl;
  
  if( m_Variable == "pt"   ){  cw = (int*) 1200; ch = (int*) 1000; }
  if( m_Variable == "cts"  ){  cw = (int*) 1200; ch = (int*) 1000; }
  if( m_Variable == "njet" ){  cw = (int*) 1200; ch = (int*) 1000; }
  if( m_Variable == "m12"  ){  cw = (int*) 1200; ch = (int*) 1000; }
  if( m_Variable == "m34"  ){  cw = (int*) 1200; ch = (int*) 1000; }
  if( m_Variable == "y"    ){  cw = (int*) 1200; ch = (int*) 1000; }
  if( m_Variable == "ljpt" ){  cw = (int*) 1200; ch = (int*) 1000; }
  if( m_Variable == "ljpt" ){  cw = (int*) 1200; ch = (int*) 1000; }
  else cw = (int*) 1200; ch = (int*) 1200;
  
}


void InputMaker :: SetAtlasStyle(){

  TStyle *atlasStyle= new TStyle("ATLAS","Atlas style");
  
  /*use plain black on white colors*/
  Int_t icol=0; // WHITE
  atlasStyle->SetFrameBorderMode(icol);
  atlasStyle->SetFrameFillColor(icol);
  atlasStyle->SetCanvasBorderMode(icol);
  atlasStyle->SetCanvasColor(icol);
  atlasStyle->SetPadBorderMode(icol);
  atlasStyle->SetPadColor(icol);
  atlasStyle->SetStatColor(icol);
  /*don't use: white fill color for *all* objects*/
  //atlasStyle->SetFillColor(icol); 

  /*set the paper & margin sizes*/
  atlasStyle->SetPaperSize(20,26);

  /*set margin sizes*/
  atlasStyle->SetPadTopMargin(0.05);
  atlasStyle->SetPadRightMargin(0.05);
  atlasStyle->SetPadBottomMargin(0.16);
  atlasStyle->SetPadLeftMargin(0.14);

  /*set title offsets (for axis label)*/
  atlasStyle->SetTitleXOffset(1.4);
  atlasStyle->SetTitleYOffset(1.4);

  /*use large fonts*/
  //Int_t font=72; // Helvetica italics
  Int_t font=42; // Helvetica
  Double_t tsize=0.04; //io=0.04
  atlasStyle->SetTextFont(font);

  atlasStyle->SetTextSize(tsize);
  atlasStyle->SetLabelFont(font,"x");
  atlasStyle->SetTitleFont(font,"x");
  atlasStyle->SetLabelFont(font,"y");
  atlasStyle->SetTitleFont(font,"y");
  atlasStyle->SetLabelFont(font,"z");
  atlasStyle->SetTitleFont(font,"z");

  atlasStyle->SetLabelSize(tsize,"x");
  atlasStyle->SetTitleSize(tsize,"x");
  atlasStyle->SetLabelSize(tsize,"y");
  atlasStyle->SetTitleSize(tsize,"y");
  atlasStyle->SetLabelSize(tsize,"z");
  atlasStyle->SetTitleSize(tsize,"z");

  /*use bold lines and markers*/
  atlasStyle->SetMarkerStyle(20);
  atlasStyle->SetMarkerSize(1.2);
  atlasStyle->SetHistLineWidth(2.);
  atlasStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes

  /*get rid of X error bars*/
  //atlasStyle->SetErrorX(0.001);
  /*get rid of error bar caps*/
  atlasStyle->SetEndErrorSize(0.);

  /*do not display any of the standard histogram decorations*/
  atlasStyle->SetOptTitle(0);
  //atlasStyle->SetOptStat(1111);
  atlasStyle->SetOptStat(0);
  //atlasStyle->SetOptFit(1111);
  atlasStyle->SetOptFit(0);

  /*put tick marks on top and RHS of plots*/
  atlasStyle->SetPadTickX(1);
  atlasStyle->SetPadTickY(1);

  gROOT->SetStyle("ATLAS");
  gROOT->ForceStyle();

}

void InputMaker :: defineColors(){
  
  sigCol    = TColor::GetColor("#67B2FF");
  ZZCol     = TColor::GetColor("#E20D0A");
  ZZEWCol   = TColor::GetColor("#B5236F");
  redBkgCol = TColor::GetColor("#793FD2");
  ttVCol    = TColor::GetColor("#EDE810");
  ggFCol    = TColor::GetColor("#67B2FF");
  VBFCol    = TColor::GetColor("#99E148");
  VHCol     = TColor::GetColor("#EB9B35");
  WHCol     = TColor::GetColor("#EB9B35");
  ZHCol     = kOrange;
  ttHCol    = kBlue;
  dataCol   = kBlack;

}

//void InputMaker :: defineAxisLabelsMaps(){
//
//  /* Shape plots */
//  m_shapeLabels["pt"]     = { "p_{T} [GeV]"            , "Events / GeV"       };
//  m_shapeLabels["m12"]    = { "m_{12} [GeV]"           , "Events / GeV"       };
//  m_shapeLabels["m34"]    = { "m_{34} [GeV]"           , "Events / GeV"       };
//  m_shapeLabels["y"]      = { "|y|"                    , "Events / Bin Width" };
//  m_shapeLabels["njet"]   = { "N_{jets}"               , "Events"             };
//  m_shapeLabels["njetv2"] = { "N_{jets}"               , "Events"             };
//  m_shapeLabels["njetv3"] = { "N_{jets}"               , "Events"             };
//  m_shapeLabels["nbjet"]  = { "N_{jets}"               , "Events"             };
//  m_shapeLabels["m12m34"] = { "m_{12} vs m_{34}"       , "Events"             };
//  m_shapeLabels["cts"]    = { "|cos#theta^{*}|"        , "Events / Bin Width" };
//  m_shapeLabels["mjj"]    = { "m_{jj} [GeV]"           , "Events / GeV"       };
//  m_shapeLabels["etajj"]  = { "#Delta#eta_{jj}"        , "Events / Bin Width" };
//  m_shapeLabels["phijj"]  = { "#Delta#phi_{jj}"        , "Events / #pi"       };
//  m_shapeLabels["ljpt"]   = { "p_{T}^{lead. jet} [GeV]", "Events / GeV"       };
//  m_shapeLabels["ptpt"]   = { "p_{T} [GeV]"            , "Events / GeV"       };
//  m_shapeLabels["pt0j"]   = { "p_{T}^{0 jet} [GeV]"    , "Events / GeV"       };
//  m_shapeLabels["pt1j"]   = { "p_{T}^{1 jet} [GeV]"    , "Events / GeV"       };
//  m_shapeLabels["pt2j"]   = { "p_{T}^{>=2 jet} [GeV]"  , "Events / GeV"       };
//  
//  /* Sigma Unfolded plots */
//  m_sigUnfLabels["pt"]     = { "p_{T} [GeV]"             , "#sigma / Bin Width [fb/GeV]" };
//  m_sigUnfLabels["m12"]    = { "m_{12} [GeV]"            , "#sigma / Bin Width [fb/GeV]" };
//  m_sigUnfLabels["m34"]    = { "m_{34} [GeV]"            , "#sigma / Bin Width [fb/GeV]" };
//  m_sigUnfLabels["y"]      = { "|y|"                     , "#sigma / Bin Width [fb]"     };
//  m_sigUnfLabels["njet"]   = { "N_{jets}"                , "#sigma [fb]"                 };
//  m_sigUnfLabels["njetv2"] = { "N_{jets}"                , "#sigma [fb]"                 };
//  m_sigUnfLabels["njetv3"] = { "N_{jets}"                , "#sigma [fb]"                 };
//  m_sigUnfLabels["nbjet"]  = { "N_{b-jets}"              , "#sigma [fb]"                 };
//  m_sigUnfLabels["m12m34"] = { "m_{12} vs m_{34}"        , "#sigma [fb]"                 };
//  m_sigUnfLabels["cts"]    = { "|cos#theta^{*}|"         , "#sigma / Bin Width [fb]"     };
//  m_sigUnfLabels["mjj"]    = { "m_{jj} [GeV]"            , "#sigma / Bin Width [fb/GeV]" };
//  m_sigUnfLabels["etajj"]  = { "#Delta#eta_{jj}"         , "#sigma / Bin Width [fb]"     };
//  m_sigUnfLabels["phijj"]  = { "#Delta#phi_{jj}"         , "#sigma / #pi [fb]"           };
//  m_sigUnfLabels["ljpt"]   = { "p_{T}^{lead. jet} [GeV]" , "#sigma / Bin Width [fb/GeV]" };
//  m_sigUnfLabels["ptpt"]   = { "p_{T} [GeV]"             , "#sigma / Bin Width [fb/GeV]" };
//  m_sigUnfLabels["pt2j"]   = { "p_{T, >=2 jets} [GeV]"   , "#sigma / Bin Width [fb/GeV]" };
//  
//}
