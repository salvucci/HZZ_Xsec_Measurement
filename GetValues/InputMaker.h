#ifndef InputMaker_H
#define InputMaker_H

/* C++ includes */
#include "iostream"
#include "set"
#include "map"
#include <string>
#include <sstream>
#include <fstream>
#include <numeric>
#include <unistd.h>
#include <sys/stat.h>
#define GetCurrentDir getcwd

/* ROOT includes */
#include <TH1.h>
#include <TH2.h>
#include <TObject.h>
#include <TTree.h>
#include <TFile.h>
#include <TF1.h>
#include <TLine.h>
#include <TGraphAsymmErrors.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TColor.h>
#include <TGraph.h>
#include <TMath.h>
#include <TChain.h>
#include <TLatex.h>
#include <TIterator.h>
#include <TLegend.h>

/* others */
#include "Enum.h"

using namespace std;

class InputMaker {

 public:
  
  /****************/
  /* Constructors */
  /****************/
  InputMaker(string ProdMode, string Variable, string Channel,
				 string RecoFile, string TruthFile,
				 string InputDir, string OutputDir,
				 bool doFiducial, bool doFactors, bool doMatrix, bool doComparison,
				 bool doPlots, bool doDressed, bool debug, bool verbose,
				 bool checkTaus, bool checkJets);
    
  /****************/
  /*   Methods    */
  /****************/

  /* Initialize some things */
  void InitializeIO();
  void Initialize();
  void setBinning();
  void setBins();
  void setXS();
  void setBR();
  void setChains(TString prodMode);

  /* Load Objects */
  void LoadObject();

  /* Load Input Values  -- functions defined in InputValues.C */
  void LoadAllFactors();
  void LoadCFactors();
  void LoadAFactors();
  void LoadRFactors();
  void LoadMinitreeN();
  void LoadNbinbybin();
  void LoadfNonfid();
  void LoadRspMtx();
  void LoadValues(vector<vector<double>> &vec);
  void LoadValues(vector<double> &vec);
  void LoadValues(double**& mtx);
  void PrintValues(vector<double> &vals, bool save);
  void PrintValues(double**& mtx, bool save);

  /* Calculate A, C, r factors and Nbinbybin -- functions defined in getFactors.C */
  void getFactors();
  void calcNbinbybin();
  void calcFloatFactors(); 
  void calcIntFactors(); 
  void writeFactors();

  /* Calculate response matrix, fnonfid -- functions in responseMatrix.C */
  void responseMatrixInt();
  void responseMatrixFloat();
  void writeMatrix();
  void checkJets();

  /* Test closure*/
  void fillNrspmtx();
  void printClosure();
  void compareCFmtx();

  /* Some useful things -- in Utils.cxx */
  void initializeFactorVectors();
  void initializeMatrixVectors();
  int getBin(float val);
  TString GetFidMinitreeVar();
  TString GetRecoMinitreeVar();
  void LoadInputFile();
  void DefineInputData();
  void CreateOutputDir();
  void CreateProcList();
  string GetCurrentWorkingDir();

  /* Plotting funcitons */
  void plotMatrix(bool doNorm);
  void plotMatrix(double**& mtx, TString name);
  void plotRatio();

  /* Cleaning */
  void CleanUp();

  string ConvertToStringWithPrecision(double value, int n=2);
  void drawlatex(TLatex *t);
  void DrawSigmaLines (TF1 *sigma);

  /* Run the Class */
  void RunClass();

  /*Get Channel/Process and Variable Name/Identifier */
  inline const char* GetChannel(int n);
  inline const char* GetProcess(int p);
  string GetVariable(int v);
  int GetChannelId(string ch);

  void SetCanvasSize(int *cw, int *ch);
  
  /* Set Style */
  void SetAtlasStyle();
  void drawATLASInternal();
  void drawEnergyLumi();
  void drawChannel();
  void defineColors();
  
  /* define axis labels*/
  void defineAxisLabelsMaps();

  /* Close the class */
  void Closing();
  
 protected:
  /* class options */
  string m_ProdMode;
  string m_RecoFile;
  string m_TruthFile;
  string m_Variable;
  string m_Channel;
  bool m_doFiducial;
  bool m_doFactors;
  bool m_doMatrix;
  bool m_doComparison;
  bool m_doPlots;
  bool m_doDressed;
  bool m_checkTaus;
  bool m_checkJets;
  bool m_debug;
  bool m_verbose;

  /*****************/
  /* class members */
  /*****************/
  
 private:
  string m_OutputDir;
  string m_InputDir;
  TChain* m_RecoChain;
  TChain* m_FidChain;
  int m_ChNum;
  int m_binnum;
  string m_Dir;
  vector<float> m_bins;
  vector<char*> m_BinLabels;
  double m_lumi;
  double m_BR;
  double m_XS;
  vector<double> m_aFactors;
  vector<double> m_cFactors;
  vector<double> m_rFactors;
  vector<double> m_Nminitree;
  vector<double> m_Nbinbybin;
  vector<double> m_Nrspmtx;
  vector<double> m_fNonfid;
//  map<int, vector<pair<double, double>>> m_aFactors, m_cFactors, m_rFactors, m_Nminitree, m_Nbinbybin, m_Nrspmtx, m_fNonfid;
  string m_cfFile, m_afFile, m_rfFile, m_minitreeNFile, m_binbybinNFile, m_rspmtxNFile, m_fNfFile, m_rspMtxFile;
  //vector<vector<double>*> m_rspMtx;
  //double m_rspMtx[11][11];
  double** m_rspMtx;
  double** m_cFmtx;
  std::ifstream m_InStream;
  std::ofstream m_OutStream;

  /* process colors */
  Int_t sigCol, ZZCol, ZZEWCol, redBkgCol, ttVCol, dataCol;
  Int_t ggFCol, VBFCol, VHCol, WHCol, ZHCol, ttHCol;

};
#endif
